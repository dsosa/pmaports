# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=qt5-qtsystems
pkgver=0_git20180122
_commit="66e45676f5f5c6251ccab36906c6324fd0065e08"
pkgrel=0
arch="x86_64"
url='http://qt-project.org/'
license="LGPL-3.0"
pkgdesc='Qt Systems'
depends=""
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev git"
source="$pkgname-$_commit.tar.gz::https://github.com/qt/${pkgname/qt5-/}/archive/$_commit.tar.gz"
subpackages="$pkgname-dev $pkgname-doc"
builddir="$srcdir/${pkgname/qt5-/}-$_commit"
options="!check"

prepare() {
	default_prepare
	# Something in the build system only runs the syncqt.pl script when it's in a git repo
	git init
}

build() {
	cd "$builddir"
	qmake-qt5
	make
}

package() {
	cd "$builddir"
	make INSTALL_ROOT="$pkgdir" install

	# Drop QMAKE_PRL_BUILD_DIR because reference the build dir
	find "$pkgdir/usr/lib" -type f -name '*.prl' \
		-exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

	install -d "$pkgdir"/usr/share/licenses
	ln -s /usr/share/licenses/qt5-base "$pkgdir"/usr/share/licenses/$pkgname
}
sha512sums="b0190cd2b02d41222738887ef24d1f34aa74e3d739fe5eb61b02a23369f43b718bf92c08b4a083e703598443d2445a36cf60980d9ef31ac71303d07bb7d95de7  qt5-qtsystems-66e45676f5f5c6251ccab36906c6324fd0065e08.tar.gz"
