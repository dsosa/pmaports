# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/librem5-evk_defconfig

pkgname="linux-purism-librem5dev"
pkgver=4.18.11
pkgrel=0
pkgdesc="Purism Librem 5 devkit kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5dev"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_repository="linux-emcraft"
_commit="dbabc7952b22bcbe5911c20541ec2ba60393d9da"
_config="config-${_flavor}.${arch}"
source="
	$pkgname-$_commit.tar.gz::https://source.puri.sm/Librem5/${_repository}/-/archive/${_commit}.tar.gz
	$_config
"
builddir="$srcdir/${_repository}-${_commit}"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		downstreamkernel_prepare "$srcdir" "$builddir" "$_config" "$_carch" "$HOSTCC"
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	# kernel.release
	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# zImage (find the right one)
	cd "$builddir/arch/$_carch/boot"
	_target="$pkgdir/boot/vmlinuz-$_flavor"
	for _zimg in zImage-dtb Image.gz-dtb *zImage Image; do
		[ -e "$_zimg" ] || continue
		msg "zImage found: $_zimg"
		install -Dm644 "$_zimg" "$_target"
		break
	done
	if ! [ -e "$_target" ]; then
		error "Could not find zImage in $PWD!"
		return 1
	fi
	cd "$builddir"
	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}

sha512sums="870d63ada890901e2b6d3249ba9ca252bf90c7f03893cc44295eed787188ddb9e7ef485595fa2539692b5a7a55cba0fd65ae356946fcd65693599ed0a22d38ae  linux-purism-librem5dev-dbabc7952b22bcbe5911c20541ec2ba60393d9da.tar.gz
e8871767aac7895de98769e70b6eef791a78d3eb41c73570e35dd82795e89f1da2eaa96014abf54c50ba6bae55ac02c1aea19ca40a0ded3bd23560fdedb3e932  config-purism-librem5dev.aarch64"
